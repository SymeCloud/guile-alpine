FROM alpine:latest

RUN apk add --no-cache guile

CMD ["guile"]
